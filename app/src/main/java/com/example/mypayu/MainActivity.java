package com.example.mypayu;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.payu.base.models.ErrorResponse;
import com.payu.base.models.PayUPaymentParams;
import com.payu.checkoutpro.PayUCheckoutPro;
import com.payu.checkoutpro.utils.PayUCheckoutProConstants;
import com.payu.ui.model.listeners.PayUCheckoutProListener;
import com.payu.ui.model.listeners.PayUHashGenerationListener;

import java.security.Key;
import java.security.MessageDigest;
import java.util.HashMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class MainActivity extends AppCompatActivity {

    Button pay_now;
    String testMerchantSecretKey = "add your merchnat secret key";
    String testMerchantSecretSalt = "add your merchant salt";
//    please replace your success and failurl
    String successUrl = "https://payuresponse.firebaseapp.com/success";
    String failUrl = "https://payuresponse.firebaseapp.com/failure";
//    for production make setIsProduction to True
    Boolean setIsProduction = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pay_now = findViewById(R.id.pay_now);

        pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> additionalParams = new HashMap<>();
                additionalParams.put(PayUCheckoutProConstants.CP_UDF1, "udf1");
                additionalParams.put(PayUCheckoutProConstants.CP_UDF2, "udf2");
                additionalParams.put(PayUCheckoutProConstants.CP_UDF3, "udf3");
                additionalParams.put(PayUCheckoutProConstants.CP_UDF4, "udf4");
                additionalParams.put(PayUCheckoutProConstants.CP_UDF5, "udf5");

                PayUPaymentParams.Builder builder = new PayUPaymentParams.Builder();
                builder.setAmount("10")
                .setIsProduction(setIsProduction)
                .setProductInfo("Test")
                .setKey(testMerchantSecretKey)
                .setPhone("9877691416")
                .setTransactionId(String.valueOf(System.currentTimeMillis()))
                .setFirstName("shubham")
                .setEmail("thakurshubham156@gmail.com")
                .setSurl(successUrl)
                .setFurl(failUrl)
                .setUserCredential("shubham")
                .setAdditionalParams(additionalParams);
                PayUPaymentParams payUPaymentParams = builder.build();
                initUiSdk(payUPaymentParams);



            }
        });
    }

    private void initUiSdk(PayUPaymentParams payUPaymentParams) {
        PayUCheckoutPro.open(
                this,
                payUPaymentParams,
                new PayUCheckoutProListener() {

                    @Override
                    public void onPaymentSuccess(Object response) {
                        //Cast response object to HashMap
                        HashMap<String,Object> result = (HashMap<String, Object>) response;
                        String payuResponse = (String)result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
                        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);
                        Log.i("pay_success1",payuResponse);
                        Log.i("pay_success2",payuResponse);

                        Toast.makeText(getApplicationContext(),payuResponse,Toast.LENGTH_SHORT).show();

                    }


                    @Override
                    public void onPaymentFailure(Object response) {
                        //Cast response object to HashMap
                        HashMap<String,Object> result = (HashMap<String, Object>) response;
                        String payuResponse = (String)result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
                        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);

                        Log.i("pay_success1",payuResponse);
                        Log.i("pay_success2",payuResponse);
                        Toast.makeText(getApplicationContext(),payuResponse,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPaymentCancel(boolean isTxnInitiated) {
                        Toast.makeText(getApplicationContext(),String.valueOf(isTxnInitiated),Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(ErrorResponse errorResponse) {
                        String errorMessage = errorResponse.getErrorMessage();
                        Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void setWebViewProperties(@Nullable WebView webView, @Nullable Object o) {
                        //For setting webview properties, if any. Check Customized Integration section for more details on this
                    }

                    @Override
                    public void generateHash(HashMap<String, String> valueMap, PayUHashGenerationListener hashGenerationListener) {
                        String hashName = valueMap.get(PayUCheckoutProConstants.CP_HASH_NAME);
                        String hashData = valueMap.get(PayUCheckoutProConstants.CP_HASH_STRING);
                        if (!TextUtils.isEmpty(hashName) && !TextUtils.isEmpty(hashData)) {
                            //Generate Hash from your backend here
                            String hash = null;
                            if (hashName.equalsIgnoreCase(PayUCheckoutProConstants.CP_LOOKUP_API_HASH)){
                                //Calculate HmacSHA1 HASH for calculating Lookup API Hash
                                ///Do not generate hash from local, it needs to be calculated from server side only. Here, hashString contains hash created from your server side.

                                hash = calculateHmacSHA1Hash(hashData, testMerchantSecretKey);
                            } else {

                                //Calculate SHA-512 Hash here
                                hash = calculateHash(hashData + testMerchantSecretSalt);
                            }

                            HashMap<String, String> dataMap = new HashMap<>();
                            dataMap.put(hashName, hash);
                            hashGenerationListener.onHashGenerated(dataMap);
                        }
                    }
                }
        );
    }
    private String calculateHmacSHA1Hash(String data, String key) {
        String HMAC_SHA1_ALGORITHM = "HmacSHA1";
        String result = null;

        try {
            Key signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
            result = getHexString(rawHmac);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private String calculateHash(String hashString) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
            messageDigest.update(hashString.getBytes());
            byte[] mdbytes = messageDigest.digest();
            return getHexString(mdbytes);
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    private String getHexString(byte[] array){
        StringBuilder hash = new StringBuilder();
        for (byte hashByte : array) {
            hash.append(Integer.toString((hashByte & 0xff) + 0x100, 16).substring(1));
        }
        return hash.toString();
    }



}